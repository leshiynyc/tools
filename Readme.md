
# .vimrc and vundle ##

copy or move .vimrc to your home directory

    cp -v .vimrc ~/

# Install vundle for vim #

1. `git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle`

2. Use vundle to install vim plugins/extensions

run `:BundleInstall` from within vim

or  `vim -c ":BundleInstall" ` from the command line

Once the above is done, extra syntax and other misc packages for vim will be installed.

# Git helper scripts and configuration files #

1. **.gitconfig**

copy this file to your home directory
`cp -v .gitconfig ~/` and then open an editor and add your name and email address.

2. **.git-prompt.sh**

Once again copy this file to your home directory and give it exec permissions

`cp -v .git-prompt.sh ~/ && chmod +x ~/.git-prompt.sh`

then run `cat add.2.bashrc >> ~/.bashrc && source ~/.bashrc `

The above will tailor the prompt to our needs. It will show the current working directory right above the actual prompt and your working branch, if with in a git repo branch. 
Similar to this:

    [/cygdrive/c/Users/sk4020/Documents/projects/tools]

    sk4020@082-SK4020-L1]  (master) #
